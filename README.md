# Todo List

The Todo List is a single page application written on React using Typescript.

# Visual
<img src='./src/img/img.png'>

# What's included

The project is wrote on Visual Studio Code and using:

- HTML
- CSS
- React
- Typescript
