import {ChangeEvent, FC, useState} from 'react';
import './App.css';
import {ITask} from './interfaces';
import TodoTask from './Components/TodoTask';
import { Dayjs } from 'dayjs';
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import Button from '@mui/material/Button';
const App: FC = () => {
  const [task, setTask] = useState<string>("");
  const [dateWithNoInitialValue, setDateWithNoInitialValue] = useState<Dayjs | null>(null);
  const [todoList, setTodoList] = useState<ITask[]>([]);
  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    if(event.target.name === 'task'){
      setTask(event.target.value)
    }
  }
  const addTask = (): void => { 
    const newTask = {taskName: task, deadLine: dateWithNoInitialValue}
    setTodoList([...todoList, newTask]);
    setTask('');
    setDateWithNoInitialValue(null);
  }
  const completeTask = (taskNameToDelete: string) : void => {
    setTodoList(todoList.filter(task => { 
      return task.taskName !== taskNameToDelete
    }));
  } 
  return (
    <div className="App">
      <div className="header">
        <div className='inputContainer'>
        <TextField type="text" name='task' value={task} placeholder='Task...' onChange={handleChange} className='textInput'/>
        <LocalizationProvider dateAdapter={AdapterDayjs} className='dateInput'>
        <DateTimePicker
          value={dateWithNoInitialValue}
          onChange={(newValue) => setDateWithNoInitialValue(newValue)}
          renderInput={(params) => (
            <TextField {...params} />
          )}
        />
      </LocalizationProvider>
        </div>
        <Button onClick={addTask}>Add Task</Button>
      </div>
      <div className="todoList">
        {todoList.map((task: ITask, key: number) => {
          return <TodoTask key={key} task={task} completeTask={completeTask}/>
        })}
      </div>
    </div>
  );
}

export default App;
