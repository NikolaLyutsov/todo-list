import { Dayjs } from 'dayjs';
export interface ITask {
    taskName: string;
    deadLine: Dayjs | null;
} 