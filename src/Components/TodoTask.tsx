import { ITask } from '../interfaces';
interface Props {
    task: ITask;
    completeTask(taskNameToDelete: string): void;
}
const TodoTask = ({task, completeTask}: Props) => {
  return (
    <div className='task'>
        <div className='content'>
        <span className='taskName'>{task.taskName}</span>
        <span>{task.deadLine?.format("h:mm A, D MMMM YYYY")}</span>
        </div>
        <button onClick={() => { completeTask(task.taskName)} }>X</button>
        </div>
  )
}

export default TodoTask;